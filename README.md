# miningless-scripts 

 usless scripts for your GNU/Linux system #
 place this scripts in /usr/local/bin!
 "chmod -x" it all!

------------------------------------------------------------------------------------------------

# system-tuner. 
by Mauro Ruben Lopez Itza.

This simple script tries to simplify the process of updating repositories and cleaning packages 
through command line adding an insignificantly elegant touch.

change the specific packages update commands for those corresponding to your distribution. 

-------------------------------------------------------------------------------------------------

# rofi_session. 
by Mauro Ruben Lopez Itza.

This simple script is made to work with "rofi". It tries to be an option or solution to handling  
sessions in minimal graphic installations that do not have an integrated session manager. Ej. I3-wm.

# dependences: "rofi".

put the "suspend_machine"; poweroff_machine"; "monitor_off" and "exit_session" scripts in place.
/usr/local/bin is prefered. 

--------------------------------------------------------------------------------------------------

# rofi_session-cripts. 
by Mauro Ruben Lopez Itza.

These scripts have a practically futile character because they can be done by any GNU / Linux user
without effort. They are included, however, as a complement to "rofi_session".

uncomment the "exit_session" script for your correspondig session comand. 
Ej. in i3-wm "i3-msg exit".

--------------------------------------------------------------------------------------------------

# feh_viewer 
by Mauro Ruben Lopez Itza.

This simple script presents a simple solution to display 
all the files contained in a single directory with "feh" from any GUI filebrowser.

---------------------------------------------------------------------------------------------------

# clima
by Mauro Ruben Lopez Itza

This small script lends utility in the most discreet way when it comes to finding out about weather conditions from the same terminal.
Dependencies: "ansiwheater"
Consult "man ansiweather" for a correct configuration

-----------------------------------------------------------------------------------------------------

# screen-manager
by Mauro Ruben Lopez Itza

This small and simple script, along with the other two complementary scripts: "lock_screen" and "lock_screen-blur" try to offer a super lightweight and simple alternative 
to configure. The script uses "ffmpeg" so this is necessarily a required dependency, as well as "i3lock". 
The script is made to be used in "rofi". But the same its two complementary scripts can be added to any other menu / keyboard shortcut or placed as launchers on the desktop.

Dependencies: "ffmpeg"; "i3lock"
