#!/bin/bash

RENAME=(`dmenu < .symbols`)

if [ "$RENAME" != "" ]; then

    bspc desktop -n "$RENAME"

else

    exit 0

    fi

## ".symbols" is a file in your home directory with symbols, ej. Font Awesome symbols ##
