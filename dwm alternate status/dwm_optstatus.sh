#!/bin/bash

disk="rootwindow_disk.sh"
mounted="rootwindow_mounted.sh"
weather="rootwindow_weather.sh"
slstatus="rootwindow_slstatus.sh"

OPTIONS="disk\nmounted\nweather\nstatus"
LAUNCHER="dmenu -i -p status"

option=`echo -e $OPTIONS | $LAUNCHER | awk '{print $1}' | tr -d '\r\n'`
if [ ${#option} -gt 0 ]
then
    case $option in
	disk)
	    $disk
	    ;;
	mounted)
	    $mounted
	    ;;
	weather)
	    $weather
	    ;;
	status)
	    $slstatus
	    ;;
       	*)
	    ;;
    esac
fi

