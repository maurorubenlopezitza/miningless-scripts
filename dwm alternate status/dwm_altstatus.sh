#!/bin/bash

SLSTATUS_PID=(`pgrep slstatus`)
ROOTWINDOW_PID=(`pgrep rootwindow`)

if [ "$SLSTATUS_PID" != "" ]; then

    kill $SLSTATUS_PID
    dwm_optstatus.sh

else

    kill $ROOTWINDOW_PID
    dwm_optstatus.sh

    fi
