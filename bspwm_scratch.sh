#!/bin/bash

CHECK=(`xdotool search --name "st-scratchpad"`)
RECHECK=(`wmctrl -l | grep st-scratchpad`)

altern_scratch() {

       if [ "$RECHECK" == "" ]; then

	   xdotool search --name "st-scratchpad" windowmap

       else

	   xdotool search --name "st-scratchpad" windowunmap

       fi

}


if [ "$CHECK" == "" ]; then

    st -T st-scratchpad -c scratchpad -g 100x30

 else

     $(altern_scratch)

     fi


